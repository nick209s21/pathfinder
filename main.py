import math
import random
from numba import jit
import pygame
from pprint import pprint


def create_canvas(size=1000, grid=50):
    canvas = []
    for x in range(0, size, grid):
        beta_canvas = []
        for y in range(0, size, grid):
            beta_canvas.append(0)
        canvas.append(beta_canvas)
    return canvas


def path(a, b, canvas, grid, moveline=False):
    def step(stepplace):
        # print('inspecting')
        for i in range(-1, 2):
            for n in range(-1, 2):
                if (abs(n) != abs(i) and moveline) or (n != i and not moveline):
                    gopos = (stepplace[0] + i, stepplace[1] + n)
                    if -1 < gopos[0] < len(canvas) and -1 < gopos[1] < len(canvas[startpos[0]]):
                        if not canvas[gopos[0]][gopos[1]]:
                            canvas[gopos[0]][gopos[1]] = (-i, -n)
                            stepers.append(gopos)
                        elif canvas[gopos[0]][gopos[1]] == 'finnish':
                            return n, i
                        pass

    startpos = (a[0] // grid, a[1] // grid)
    endpos = (b[0] // grid, b[1] // grid)
    # print(len(canvas), len(canvas[0]), startpos, endpos)
    canvas[startpos[0]][startpos[1]] = 'start'
    canvas[endpos[0]][endpos[1]] = 'finnish'
    stepers = []
    step(startpos)
    way = []
    while True:
        for m in stepers[:]:
            # print(m, 'go to inspect')
            if step(m):
                # print('found the way!', m)
                now_where = m
                while True:
                    # print(canvas)
                    way.append(now_where)
                    # print(now_where)
                    where = canvas[now_where[0]][now_where[1]]
                    # print('steel going home!', now_where, where)
                    if where == 'start':
                        # print('get home!')
                        return way
                    else:
                        now_where = (now_where[0] + where[0],
                                     now_where[1] + where[1])
                    pass
                pass
            else:
                # print('its alive', m)
                stepers.remove(m)
        if not stepers:
            # print(canvas)
            # exit()
            return stepers, stepers
            # step(startpos)


def maze_main(screen=pygame.display.set_mode((1000, 1000), pygame.RESIZABLE)):
    startpoint = (random.randint(0, screen.get_size()[0]), random.randint(0, screen.get_size()[1]))
    endpoint = (10, 50)
    clock = pygame.time.Clock()
    fps = 60
    walls = set()
    # for _ in range(random.randint(0, 15)):
    #     walls.append((random.randint(0, screen.get_size()[0]),
    #                   random.randint(0, screen.get_size()[1])))
    # walls = list(set(walls))
    cellsize = 50
    moveline = False
    way = path(startpoint, endpoint, create_canvas(size=max(screen.get_size()), grid=cellsize), cellsize)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                print('user want to quit :( 0: :() ):')
                exit()
            if event.type == pygame.KEYDOWN:

                if event.key == 114:
                    # walls = []
                    # for _ in range(random.randint(0, 20)):
                    #     walls.append((random.randint(0, screen.get_size()[0]) // cellsize * cellsize,
                    #                   random.randint(0, screen.get_size()[1]) // cellsize * cellsize))
                    # walls = list(set(walls))
                    startpoint = (random.randint(0, screen.get_size()[0]), random.randint(0, screen.get_size()[1]))
                    endpoint = (random.randint(0, screen.get_size()[0]), random.randint(0, screen.get_size()[1]))

                    print(abs(startpoint[0] // cellsize - endpoint[0] // cellsize)
                          + abs(startpoint[1] // cellsize - endpoint[1]) // cellsize)

                    print(int(math.dist([startpoint[0] // cellsize, startpoint[1] // cellsize],
                                        [endpoint[0] // cellsize, endpoint[1] // cellsize])), 'circle distance')
                    grid = create_canvas(size=max(screen.get_size()), grid=cellsize)
                    # print(walls)
                    for i in walls:
                        grid[i[0] // cellsize][i[1] // cellsize] = 1
                    way = path(startpoint, endpoint, grid, cellsize)
                    print(len(way), 'way len', end='\n\n')
                if event.key == pygame.K_l:
                    if moveline:
                        moveline = False
                    else:
                        moveline = True
        key = pygame.key.get_pressed()
        if key[pygame.K_d]:
            for i in list(walls)[:]:
                if math.dist(i, pygame.mouse.get_pos()) <= cellsize:
                    walls.remove(i)

        # print(way)
        if pygame.mouse.get_pressed()[1]:
            walls = set(walls)
            walls.update({(pygame.mouse.get_pos()[0] // cellsize * cellsize,
                           pygame.mouse.get_pos()[1] // cellsize * cellsize)})
        if pygame.mouse.get_pressed()[0]:
            if (pygame.mouse.get_pos()[0] // cellsize,
                pygame.mouse.get_pos()[1] // cellsize) != (startpoint[0] // cellsize,
                                                           startpoint[1] // cellsize):

                endpoint = pygame.mouse.get_pos()
                grid = create_canvas(size=max(screen.get_size()), grid=cellsize)

                for i in walls:
                    grid[i[0] // cellsize][i[1] // cellsize] = 1

                way = path(startpoint, endpoint, grid, cellsize, moveline=moveline)
        if pygame.mouse.get_pressed()[2]:
            if (endpoint[0] // cellsize,
                endpoint[1] // cellsize) != (pygame.mouse.get_pos()[0] // cellsize,
                                             pygame.mouse.get_pos()[1] // cellsize):
                startpoint = pygame.mouse.get_pos()
                grid = create_canvas(size=max(screen.get_size()), grid=cellsize)
                for i in walls:
                    grid[i[0] // cellsize][i[1] // cellsize] = 1
                way = path(startpoint, endpoint, grid, cellsize, moveline=moveline)
        for i in way:
            if i:
                pygame.draw.rect(screen, (200, 50, 50), (i[0] * cellsize - 15 / 2,
                                                         i[1] * cellsize - 15 / 2,
                                                         cellsize, cellsize))
                pygame.draw.rect(screen, (190, 40, 40), (i[0] * cellsize - 15 / 2,
                                                         i[1] * cellsize - 15 / 2,
                                                         cellsize, cellsize), cellsize // 10)

        for i in list(walls):
            pygame.draw.rect(screen, (70, 70, 70), (i[0] - 7.5, i[1] - 7.5,
                                                    cellsize, cellsize))
            pygame.draw.rect(screen, (50, 50, 50), (i[0] - 7.5, i[1] - 7.5,
                                                    cellsize, cellsize), cellsize // 10)

        pygame.draw.rect(screen, (100, 100, 50), (startpoint[0] // cellsize * cellsize - 15 / 2,
                                                  startpoint[1] // cellsize * cellsize - 15 / 2,
                                                  cellsize, cellsize))
        pygame.draw.rect(screen, (50, 150, 100), (endpoint[0] // cellsize * cellsize - 15 / 2,
                                                  endpoint[1] // cellsize * cellsize - 15 / 2,
                                                  cellsize, cellsize))
        pygame.display.flip()
        clock.tick(fps)
        screen.fill((30, 31, 32))
        grid = create_canvas(size=max(screen.get_size()), grid=cellsize)

        for i in walls:
            grid[i[0] // cellsize][i[1] // cellsize] = 1

        way = path(startpoint, endpoint, grid, cellsize, moveline=moveline)


if __name__ == '__main__':
    maze_main()
